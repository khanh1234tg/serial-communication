package gui;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import java.io.OutputStream;
import java.util.Scanner;
import java.io.InputStream;
import java.io.IOException;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Cursor;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Point;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Component;
import javax.swing.border.BevelBorder;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.MatteBorder;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.event.PopupMenuListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.event.PopupMenuEvent;
import javax.swing.DropMode;
import javax.swing.JTextArea;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JLabel;

public class SerialCom extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	private JTextField txtPortSettings;
	private JTextField txtComPort;
	private JTextField txtBaudRate;
	private JTextField txtDatabits;
	private JTextField txtStopBits;
	private JTextField txtParityBits;
	private JTextField txtStatus;
	private JTextField textUART_Tx;
	private JTextField txtEndLine;
	private JTextArea textUART_Rx;
	private JFrame serialPlotFrame;
	/* BEGIN DEFINE SERIAL PORT */
	public SerialPort serialPort1;
	public OutputStream outputStream1;
	private JTextField txtSerialData;
	private JScrollPane textScroll;
	String dataBuffer = "";
	int serialPlotTime = 0;
	volatile boolean RxDoneFlag = false;
	String dataSerialPlot;
	volatile int sampleInterval = 100;
	volatile boolean sampleIntervalFlag = false;
	volatile int setIntervalAgain  = 0;
	
	XYSeries series;
	XYSeriesCollection dataset;
	JFreeChart chart;
	XYPlot plot;

	NumberAxis domain;
	//
	enum TimeInterval{
		SAMPLE_10,
		SAMPLE_100,
		SAMPLE_200,
		SAMPLE_500,
		SAMPLE_1000,
		SAMPLE_2000,
		SAMPLE_5000,
		SAMPLE_10000
	};
	/* END DEFINE SERIAL PORT */
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SerialCom frame = new SerialCom();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SerialCom() {
		/* Begin JFrame */
		setTitle("Serial Monitor");
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 833, 547);
		contentPane = new JPanel();
		contentPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
		contentPane.setAutoscrolls(true);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(20, 50, 220, 214);
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.setAutoscrolls(true);
		panel.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
		panel.setName("PORT SETTINGS");
		panel.setPreferredSize(new Dimension(200, 300));
		panel.setMinimumSize(new Dimension(100, 100));
		panel.setFocusCycleRoot(true);
		panel.setFocusTraversalPolicyProvider(true);
		panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		contentPane.add(panel);
		panel.setLayout(null);

		txtPortSettings = new JTextField();
		txtPortSettings.setSelectionColor(new Color(255, 255, 255));
		txtPortSettings.setBounds(25, 7, 144, 25);
		txtPortSettings.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		txtPortSettings.setEditable(false);
		txtPortSettings.setAutoscrolls(false);
		txtPortSettings.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtPortSettings.setHorizontalAlignment(SwingConstants.CENTER);
		txtPortSettings.setText("PORT SETTINGS");
		panel.add(txtPortSettings);
		txtPortSettings.setColumns(15);

		JComboBox comboBox_comPort = new JComboBox();
		comboBox_comPort.addPopupMenuListener(new PopupMenuListener() {
			public void popupMenuCanceled(PopupMenuEvent e) {
			}

			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
			}

			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				comboBox_comPort.removeAllItems();
				SerialPort[] portLists = SerialPort.getCommPorts();
				for (SerialPort port : portLists) {
					comboBox_comPort.addItem(port.getSystemPortName());
				}
			}
		});
		comboBox_comPort.setBounds(135, 37, 75, 21);
		panel.add(comboBox_comPort);

		txtBaudRate = new JTextField();
		txtBaudRate.setBounds(25, 62, 94, 22);
		txtBaudRate.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtBaudRate.setText("BAUD RATE");
		txtBaudRate.setEditable(false);
		panel.add(txtBaudRate);
		txtBaudRate.setColumns(8);

		JComboBox comboBox_baudRate = new JComboBox();
		comboBox_baudRate
				.setModel(new DefaultComboBoxModel(new String[] { "4800", "9600", "38400", "57600", "115200" }));
		comboBox_baudRate.setBounds(135, 64, 75, 21);
		panel.add(comboBox_baudRate);

		txtDatabits = new JTextField();
		txtDatabits.setBounds(25, 89, 94, 22);
		txtDatabits.setEditable(false);
		txtDatabits.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtDatabits.setText("DATA BITS");
		panel.add(txtDatabits);
		txtDatabits.setColumns(8);

		JPanel panelScroll = new JPanel();
		panelScroll.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelScroll.setBounds(268, 10, 537, 490);
		contentPane.add(panelScroll);
		panelScroll.setLayout(null);
		textScroll = new JScrollPane();
		textScroll.setBounds(23, 32, 504, 353);
		textScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panelScroll.add(textScroll);

		textUART_Rx = new JTextArea();
		textUART_Rx.setSelectedTextColor(new Color(0, 0, 0));
		textUART_Rx.setSelectionColor(new Color(128, 255, 255));
		textUART_Rx.setForeground(new Color(255, 255, 128));
		textUART_Rx.setCaretColor(new Color(255, 255, 0));
		textScroll.setViewportView(textUART_Rx);
		textUART_Rx.setEditable(false);
		textUART_Rx.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
		textUART_Rx.setBackground(new Color(0, 0, 0));

		JComboBox comboBox_endLine = new JComboBox();
		comboBox_endLine.setBounds(120, 459, 140, 22);
		panelScroll.add(comboBox_endLine);
		comboBox_endLine.setModel(new DefaultComboBoxModel(
				new String[] { "No Line", "New Line (\\n)", "Carriage Return (\\r)", "NL + CR (\\r\\n)" }));
		comboBox_endLine.setSelectedItem("No Line");

		JButton btnSend = new JButton("SEND");
		btnSend.setBounds(442, 455, 89, 27);
		panelScroll.add(btnSend);
		btnSend.setMargin(new Insets(2, 20, 2, 20));
		btnSend.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSend.setAlignmentX(0.5f);

		JComboBox comboBox_dataBits = new JComboBox();
		comboBox_dataBits.setModel(new DefaultComboBoxModel(new String[] { "6", "7", "8" }));
		comboBox_dataBits.setBounds(135, 91, 75, 21);
		panel.add(comboBox_dataBits);

		txtStopBits = new JTextField();
		txtStopBits.setBounds(25, 116, 94, 22);
		txtStopBits.setText("STOP BITS");
		txtStopBits.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtStopBits.setEditable(false);
		panel.add(txtStopBits);
		txtStopBits.setColumns(8);

		JComboBox comboBox_stopBits = new JComboBox();
		comboBox_stopBits.setModel(new DefaultComboBoxModel(new String[] { "1", "1.5", "2" }));
		comboBox_stopBits.setBounds(135, 118, 75, 21);
		panel.add(comboBox_stopBits);

		txtParityBits = new JTextField();
		txtParityBits.setBounds(25, 143, 94, 22);
		txtParityBits.setEditable(false);
		txtParityBits.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtParityBits.setText("PARITY BITS");
		panel.add(txtParityBits);
		txtParityBits.setColumns(8);

		JComboBox comboBox_parityBits = new JComboBox();
		comboBox_parityBits.setModel(new DefaultComboBoxModel(new String[] { "No", "Even", "Odd" }));
		comboBox_parityBits.setBounds(135, 145, 75, 21);
		panel.add(comboBox_parityBits);

		txtStatus = new JTextField();
		txtStatus.setBounds(25, 175, 94, 22);
		txtStatus.setText("STATUS");
		txtStatus.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtStatus.setEditable(false);
		txtStatus.setColumns(8);
		panel.add(txtStatus);

		txtComPort = new JTextField();
		txtComPort.setBounds(25, 35, 94, 22);
		panel.add(txtComPort);
		txtComPort.setEditable(false);
		txtComPort.setText("COM PORT");
		txtComPort.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtComPort.setColumns(8);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(20, 286, 220, 43);
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(128, 128, 128), new Color(128, 128, 128),
				new Color(128, 128, 128), new Color(128, 128, 128)));
		panel_1.setPreferredSize(new Dimension(100, 80));
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		JProgressBar progressBarComStatus = new JProgressBar();
		progressBarComStatus.setBounds(135, 176, 75, 21);
		panel.add(progressBarComStatus);

		JButton btnClearRx = new JButton("CLEAR");

		btnClearRx.setActionCommand("CLEAR");
		btnClearRx.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnClearRx.setBounds(442, 386, 85, 21);
		panelScroll.add(btnClearRx);

		txtEndLine = new JTextField();
		txtEndLine.setBounds(23, 458, 94, 22);
		panelScroll.add(txtEndLine);
		txtEndLine.setText("END LINE");
		txtEndLine.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtEndLine.setEditable(false);
		txtEndLine.setColumns(8);

		JPanel serialPlotPane = new JPanel();
		serialPlotFrame = new JFrame();
		serialPlotFrame.setTitle("Serial Plot Graph GUI");
		serialPlotFrame.setSize(600, 400);
		serialPlotFrame.setLayout(new BorderLayout());
		serialPlotFrame.add(serialPlotPane, BorderLayout.NORTH);
		serialPlotFrame.setVisible(false);
		JComboBox<String> serialInterval = new JComboBox<String>();
		
		serialInterval.addItem("10 Samples");
		serialInterval.addItem("20 Samples");
		serialInterval.addItem("50 Samples");
		serialInterval.addItem("100 Samples");
		serialInterval.addItem("200 Samples");
		serialInterval.addItem("500 Samples");
		serialInterval.addItem("1000 Samples");
		serialInterval.addItem("2000 Samples");
		serialInterval.addItem("5000 Samples");
		serialInterval.addItem("10000 Samples");
		JButton btnSampleInterval = new JButton("Select");
		JButton btnClearSerialPlot = new JButton("Clear Plot");
		
		serialInterval.setVisible(true);
		serialPlotFrame.add(serialPlotPane, BorderLayout.NORTH);
		serialPlotPane.add(serialInterval, BorderLayout.NORTH);
		serialPlotPane.add(btnSampleInterval, BorderLayout.NORTH);
		serialPlotPane.add(btnClearSerialPlot, BorderLayout.NORTH);
		JButton btnConnect = new JButton("CONNECT");

		btnConnect.setBounds(10, 6, 87, 27);
		btnConnect.setMargin(new Insets(2, 8, 2, 8));
		btnConnect.setHorizontalAlignment(SwingConstants.RIGHT);
		btnConnect.setHorizontalTextPosition(SwingConstants.CENTER);
		btnConnect.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel_1.add(btnConnect);
		btnConnect.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JButton btnClose = new JButton("CLOSE");
		btnClose.setBounds(121, 6, 89, 27);
		btnClose.setMargin(new Insets(2, 20, 2, 20));
		panel_1.add(btnClose);
		btnClose.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnClose.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JPanel panel_1_1 = new JPanel();
		panel_1_1.setLayout(null);
		panel_1_1.setPreferredSize(new Dimension(100, 80));
		panel_1_1.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(128, 128, 128), new Color(128, 128, 128),
				new Color(128, 128, 128), new Color(128, 128, 128)));
		panel_1_1.setBounds(20, 384, 230, 43);
		contentPane.add(panel_1_1);

		JLabel lblNewLabel = new JLabel("SERIAL PLOTTER");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 12, 124, 22);
		panel_1_1.add(lblNewLabel);

		JButton btnSerialPlot = new JButton("PLOT");

		btnSerialPlot.setMargin(new Insets(2, 8, 2, 8));
		btnSerialPlot.setHorizontalTextPosition(SwingConstants.CENTER);
		btnSerialPlot.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSerialPlot.setEnabled(false);
		btnSerialPlot.setAlignmentX(0.5f);
		btnSerialPlot.setBounds(143, 10, 77, 27);
		panel_1_1.add(btnSerialPlot);
		
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					SerialPort[] portLists = SerialPort.getCommPorts();
					serialPort1 = portLists[comboBox_comPort.getSelectedIndex()];
					serialPort1.setBaudRate(Integer.parseInt(comboBox_baudRate.getSelectedItem().toString()));
					serialPort1.setNumDataBits(Integer.parseInt(comboBox_dataBits.getSelectedItem().toString()));
					serialPort1.setNumStopBits(Integer.parseInt(comboBox_stopBits.getSelectedItem().toString()));
					serialPort1.setParity(comboBox_parityBits.getSelectedIndex());
					serialPort1.openPort();
					serialPlotTime = 0;
					if (serialPort1.isOpen()) {
						JOptionPane.showMessageDialog(null, serialPort1.getDescriptivePortName() + " is OPEN");
						comboBox_comPort.setEnabled(false);
						progressBarComStatus.setValue(100);
						btnConnect.setEnabled(false);
						btnSend.setEnabled(true);
						btnClose.setEnabled(true);
						btnClearRx.setEnabled(true);
						btnSerialPlot.setEnabled(true);
						// Read Rx Data
						Serial_EventBaseReading(serialPort1);

					} else {
						JOptionPane.showMessageDialog(null, serialPort1.getDescriptivePortName() + " can not OPEN",
								"ERROR", ERROR_MESSAGE);
					}
				} catch (ArrayIndexOutOfBoundsException a) {
					JOptionPane.showMessageDialog(null, "No COM PORT is SELECTED", "ERROR", ERROR_MESSAGE);
				} catch (Exception b) {
					JOptionPane.showMessageDialog(null, b, "ERROR", ERROR_MESSAGE);
				}
			}
		});
		
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (serialPort1.isOpen()) {
					serialPort1.closePort();
					comboBox_comPort.setEnabled(true);
					progressBarComStatus.setValue(0);
					btnConnect.setEnabled(true);
					btnSend.setEnabled(false);
					btnClose.setEnabled(false);
					btnClearRx.setEnabled(false);
					btnSerialPlot.setEnabled(false);
					serialPlotFrame.setVisible(false);
				}
			}
		});

		/* End JFrame */

		/* Begin Initial Values */
		comboBox_baudRate.setSelectedItem("9600");
		comboBox_dataBits.setSelectedItem("8");
		comboBox_stopBits.setSelectedItem("1");
		comboBox_parityBits.setSelectedItem("No");
		;
		comboBox_comPort.setEnabled(true);
		progressBarComStatus.setValue(0);
		btnConnect.setEnabled(true);
		btnClose.setEnabled(false);
		btnClearRx.setEnabled(false);

		JTextArea textArea = new JTextArea();
		textArea.setBounds(61, 339, 5, 22);
		contentPane.add(textArea);

		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				outputStream1 = serialPort1.getOutputStream();
				String dataToSend = "";

				switch (comboBox_endLine.getSelectedIndex()) {
				case 0:
					dataToSend = textUART_Tx.getText();
					break;
				case 1:
					dataToSend = textUART_Tx.getText() + "\n";
					break;
				case 2:
					dataToSend = textUART_Tx.getText() + "\r";
					break;
				case 3:
					dataToSend = textUART_Tx.getText() + "\r\n";
					break;
				}

				try {
					outputStream1.write(dataToSend.getBytes());
					textUART_Tx.setText("");
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
		});

		btnClearRx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textUART_Rx.setText("");
				dataBuffer = "";
			}
		});

		btnSend.setEnabled(false);

		textUART_Tx = new JTextField();
		textUART_Tx.setBounds(23, 413, 506, 35);
		panelScroll.add(textUART_Tx);
		textUART_Tx.setPreferredSize(new Dimension(100, 300));
		textUART_Tx.setFont(new Font("Tahoma", Font.PLAIN, 12));
		textUART_Tx.setColumns(50);

		txtSerialData = new JTextField();
		txtSerialData.setBounds(10, 3, 112, 19);
		panelScroll.add(txtSerialData);
		txtSerialData.setDisabledTextColor(new Color(0, 0, 0));
		txtSerialData.setForeground(new Color(0, 0, 0));
		txtSerialData.setHorizontalAlignment(SwingConstants.CENTER);
		txtSerialData.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtSerialData.setEditable(false);
		txtSerialData.setEnabled(false);
		txtSerialData.setText("SERIAL DATA");
		txtSerialData.setColumns(10);

		JMenu mnFile = new JMenu("File");
		mnFile.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		mnFile.setBounds(0, 0, 111, 24);
		contentPane.add(mnFile);

		btnSerialPlot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				series = new XYSeries("Serial Plotter");
				dataset = new XYSeriesCollection(series);

				chart = ChartFactory.createXYLineChart("Data", "Time (samples)", "Amplitude", dataset);
				plot = (XYPlot) chart.getPlot();
				plot.setDomainCrosshairVisible(true);
				plot.setRangeCrosshairVisible(true);
				domain = (NumberAxis) plot.getDomainAxis();
				
		        domain.setRange(0, sampleInterval);
		        domain.setTickUnit(new NumberTickUnit(sampleInterval/10));
		        //domain.setVerticalTickLabels(true);
		        //NumberAxis range = (NumberAxis) plot.getRangeAxis();
		        //range.setRange(0, 100);
		        //range.setTickUnit(new NumberTickUnit(1));
				serialPlotFrame.add(new ChartPanel(chart), BorderLayout.CENTER);
				serialPlotFrame.setVisible(true);
				serialPlotFrame.repaint();
				// create a new thread that listens for incoming text and populates the graph
				Thread thread = new Thread() {
					@Override
					public void run() {	
							while(true) {
									while(!RxDoneFlag);
									//dataSerialPlot = scanner.next();
									try {
										double dataPlot = Double.parseDouble(dataSerialPlot);
										series.add(serialPlotTime++, dataPlot);
										serialPlotFrame.repaint();
										if(serialPlotTime > sampleInterval) {
											domain.setRange(serialPlotTime-sampleInterval + 1, serialPlotTime+1);
										}
										if(sampleIntervalFlag) {
											sampleIntervalFlag = false;
											domain.setRange(serialPlotTime, serialPlotTime+sampleInterval);
											domain.setTickUnit(new NumberTickUnit(sampleInterval/10));
											
										}
									} catch(Exception e) {
									}
									
									RxDoneFlag = false;
									dataSerialPlot = "";
								}
							
						}
					};
				thread.start();
			}
		});
		btnSampleInterval.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//serialPlotTime = 0;
				//domain.setRange(serialPlotTime, serialPlotTime+sampleInterval);
				//domain.setTickUnit(new NumberTickUnit(sampleInterval/10));
				sampleInterval = updateSampleInterval(serialInterval.getSelectedIndex());
				sampleIntervalFlag = true;
				
			}
		});
		// contentPane.add(textUART_Rx);
		btnClearSerialPlot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//dataset.removeSeries(0);
				serialPlotTime = 0;
				dataset.getSeries(0).clear();
				domain.setRange(0, sampleInterval);
		        domain.setTickUnit(new NumberTickUnit(sampleInterval/10));
		        serialPlotFrame.repaint();
			}
		});
		/* End Initial Values */
	}

	public void Serial_EventBaseReading(SerialPort activePort) {
		activePort.addDataListener(new SerialPortDataListener() {

			@Override
			public int getListeningEvents() {
				return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
			}

			@Override
			public void serialEvent(SerialPortEvent event) {
				if (!serialPlotFrame.isVisible()) {
					byte[] RxData = event.getReceivedData();
					for (int i = 0; i < RxData.length; i++) {
						dataBuffer += (char) RxData[i];
						textUART_Rx.setText(dataBuffer);
					}
				}
				else if(!RxDoneFlag && serialPlotFrame.isVisible()) {
						
						byte[] RxData = event.getReceivedData();
						for (int i = 0; i < RxData.length; i++) {
							if(RxData[i] != '\r' && RxData[i] != '\n' && RxData[i] != '\0') {
								dataSerialPlot += (char) RxData[i];
							}
							else {
								RxDoneFlag = true;
								break;
							}
						}
				}
			}

		});
		
		
	}
	
	public int updateSampleInterval(int timeIndex) {
		switch(timeIndex) {
			case 0:
				return 10;
			case 1:
				return 20;
			case 2:
				return 50;
			case 3:
				return 100;
			case 4:
				return 200;
			case 5:
				return 500;
			case 6:
				return 1000;
			case 7:
				return 2000;
			case 8:
				return 5000;
			case 9:
				return 10000;
			default:
				return 10;
		}
	}
}
